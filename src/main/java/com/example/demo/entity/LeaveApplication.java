package com.example.demo.entity;

import lombok.Data;

@Data
public class LeaveApplication {
    private String id;

    /**
    * 流程实例id
    */
    private String processInstanceId;

    /**
    * 申请人用户id
    */
    private String userId;

    /**
    * 请假申请原因
    */
    private String reason;

    /**
    * 申请人姓名
    */
    private String name;

    /**
    * 流程状态（0：申请中；1：审批中；2：审批通过；3：审批不通过）
    */
    private String processStatus;

    /**
    * 请假天数
    */
    private int day;

}