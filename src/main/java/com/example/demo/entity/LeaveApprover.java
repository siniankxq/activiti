package com.example.demo.entity;

import lombok.Data;

@Data
public class LeaveApprover {
    /**
    * id
    */
    private String id;

    /**
    * 流程实例id
    */
    private String processInstanceId;

    /**
    * Activti任务id
    */
    private String taskId;

    /**
    * 审批人用户id
    */
    private String userId;

    /**
    * 审批结果（0：不通过；1：通过）
    */
    private String result;

    /**
    * 备注
    */
    private String remark;

    /**
    * 审批人姓名
    */
    private String name;
}