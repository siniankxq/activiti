package com.example.demo.mapper;

import com.example.demo.entity.LeaveApprover;

public interface LeaveApproverMapper {
    int deleteByPrimaryKey(String id);

    int insert(LeaveApprover record);

    int insertSelective(LeaveApprover record);

    LeaveApprover selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(LeaveApprover record);

    int updateByPrimaryKey(LeaveApprover record);
}