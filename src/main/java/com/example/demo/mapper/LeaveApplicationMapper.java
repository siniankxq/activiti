package com.example.demo.mapper;

import com.example.demo.entity.LeaveApplication;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LeaveApplicationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LeaveApplication record);

    int insertSelective(LeaveApplication record);

    LeaveApplication selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LeaveApplication record);

    int updateByPrimaryKey(LeaveApplication record);
}